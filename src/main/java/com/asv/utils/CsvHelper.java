package com.asv.utils;

import org.springframework.stereotype.Component;

import com.asv.entities.Backup;
import com.asv.entities.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

@Component
public class CsvHelper {

    public String backupToCsvString(Backup backup) throws JsonProcessingException {
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.builder()
                .addColumn("username")
                .addColumn("id")
                .addColumn("subject")
                .addColumn("dueDate")
                .addColumn("done")
                .build()
                .withHeader();
        new SimpleModule().addSerializer(User.class, new UserCsvSerializer());
        mapper.registerModule(new SimpleModule().addSerializer(User.class, new UserCsvSerializer()));
        return mapper.writer().with(schema).writeValueAsString(backup.getUsers());
    }
}
