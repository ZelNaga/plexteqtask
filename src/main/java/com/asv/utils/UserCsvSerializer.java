package com.asv.utils;

import java.io.IOException;

import com.asv.entities.Todo;
import com.asv.entities.User;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class UserCsvSerializer extends JsonSerializer<User> {
    @Override
    public void serialize(User toSerialize, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        for (Todo todo : toSerialize.getTodos()) {
            gen.writeObject(toSerialize.getUsername());
            gen.writeObject(todo);
        }
    }
}
