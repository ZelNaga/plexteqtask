package com.asv.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Entity
public class User {
    @Id
    private int id;
    private String username;
    private String email;
    @OneToMany(cascade= CascadeType.ALL)
    private List<Todo> todos;
}
