package com.asv.entities;

public enum BackupStatus {
    IN_PROGRESS,
    OK,
    FAILED
}
