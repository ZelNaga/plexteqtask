package com.asv.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Entity
public class Todo {
   @Id
   private int id;
   private String subject;
   private String dueDate;
   private boolean done;
   @ManyToOne
   @JsonIgnore
   private User user;
}
