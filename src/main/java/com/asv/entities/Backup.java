package com.asv.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.asv.utils.Views;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(Include.NON_NULL)
@Entity
public class Backup {
    @JsonView(Views.Id.class)
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int backupId;
    @JsonView(Views.IdAndDateAndName.class)
    private LocalDate date;
    @JsonView(Views.IdAndDateAndName.class)
    private BackupStatus status;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "backup_user",
               joinColumns = { @JoinColumn(name = "user_id") },
               inverseJoinColumns = { @JoinColumn(name = "backup_id") })
    private List<User> users;
}
