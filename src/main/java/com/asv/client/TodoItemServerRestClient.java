package com.asv.client;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import com.asv.entities.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Component
public class TodoItemServerRestClient {
    private static final String REST_SERVICE_URI = "http://localhost:9000";

    static {
        Unirest.setObjectMapper(new com.mashape.unirest.http.ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Retryable(backoff = @Backoff(delay = 10000))
    public int createOrUpdateUser(User user) throws UnirestException {
        return Unirest.post(REST_SERVICE_URI+"/users").body(user).asJson().getStatus();
    }

    @Retryable(backoff = @Backoff(delay = 10000))
    public List<User> getAllUsers() throws UnirestException {
        return Arrays.asList(Unirest.get(REST_SERVICE_URI + "/users").asObject(User[].class).getBody());
    }

    @Retryable(backoff = @Backoff(delay = 10000))
    public User getUser(int id) throws UnirestException {
        return Unirest.get(REST_SERVICE_URI + "/users/" + id).asObject(User.class).getBody();
    }

    @Retryable(backoff = @Backoff(delay = 10000))
    public int deleteUser(int id) throws UnirestException {
        return Unirest.delete(REST_SERVICE_URI + "/users/" + id).asJson().getStatus();
    }
}
