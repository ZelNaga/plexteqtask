package com.asv;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.listener.RetryListenerSupport;

import com.asv.client.TodoItemServerRestClient;
import com.asv.entities.Todo;
import com.asv.entities.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@EnableRetry
public class BackupApplication {
    @Autowired
    AppProperties properties;

    public static void main(String[] args) {
        SpringApplication.run(BackupApplication.class, args);
    }

    @Bean
    public CommandLineRunner fillTestData(TodoItemServerRestClient restClient) {
        return args -> {
            int todoId = 1;

            for (int i = 1; i <= Integer.parseInt(properties.getUsersNumber()); i++) {
                String userName = String.format("TestUser%s", i);
                String userEmail = String.format("test.user%s@test.com", i);


                List<Todo> todos = new LinkedList<>();
                for (int j = 1; j <= Integer.parseInt(properties.getTodosPerUserNumber()); j++) {
                    todos.add(Todo.builder()
                                  .id(todoId++).subject(String.format("Todo #%s for %s", todoId, userName))
                                  .dueDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                                  .done(false)
                                  .build());
                }

                restClient.createOrUpdateUser(User.builder().id(i).username(userName).email(userEmail).todos(todos).build());
            }
        };
    }

    @Bean
    public List<RetryListener> retryListeners() {

        return Collections.singletonList(new RetryListenerSupport() {
            @Override
            public <T, E extends Throwable> void onError(
                    RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
                log.warn("Retryable method {} threw {}th exception {}",
                        context.getAttribute("context.name"),
                        context.getRetryCount(), throwable.toString());
            }
        });
    }
}
