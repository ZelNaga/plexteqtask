package com.asv;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Configuration
@Getter
@Setter
public class AppProperties {
    @Value("${users.number}")
    String usersNumber;
    @Value("${todos.per.user.number}")
    String todosPerUserNumber;
}
