package com.asv.service;

import java.util.List;

import com.asv.entities.Backup;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface BackupService {
    Backup makeBackup();
    List<Backup> getBackups();
    String exportBackup(int backupId) throws JsonProcessingException;
}
