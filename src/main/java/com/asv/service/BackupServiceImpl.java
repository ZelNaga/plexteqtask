package com.asv.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.asv.client.TodoItemServerRestClient;
import com.asv.dao.BackupServiceDao;
import com.asv.entities.Backup;
import com.asv.entities.BackupStatus;
import com.asv.utils.CsvHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class BackupServiceImpl implements BackupService {
    @Autowired
    private BackupServiceDao serviceDao;
    @Autowired
    private CsvHelper csvHelper;
    @Autowired
    private TodoItemServerRestClient restClient;

    @Override
    public Backup makeBackup() {
        Backup newBackup = serviceDao.saveBackup(Backup.builder().status(BackupStatus.IN_PROGRESS).build());
        runAsyncJob(newBackup);
        return newBackup;
    }

    @Override
    public List<Backup> getBackups() {
        return serviceDao.getBackups();
    }

    @Override
    public String exportBackup(int backupId) throws JsonProcessingException {
        return csvHelper.backupToCsvString(serviceDao.getBackup(backupId)
                .orElseThrow(() -> new IllegalArgumentException("Cant find backup with id:" + backupId)));
    }

    private void runAsyncJob(Backup backup) {
        CompletableFuture.runAsync(() -> {
            try {
                //imitation of long and hard work
                TimeUnit.SECONDS.sleep(10);
                backup.setUsers(restClient.getAllUsers());
                backup.setStatus(BackupStatus.OK);
            }
            catch (UnirestException e) {
                backup.setStatus(BackupStatus.FAILED);
                log.error("Error during data reading", e);
            }
            catch (InterruptedException e) {
                backup.setStatus(BackupStatus.FAILED);
                log.error("Async work interrupted", e);
            } catch (Exception e) {
                backup.setStatus(BackupStatus.FAILED);
                log.error("Error during backup", e);
            } finally {
                serviceDao.updateBackup(backup);
            }
        });
    }
}
