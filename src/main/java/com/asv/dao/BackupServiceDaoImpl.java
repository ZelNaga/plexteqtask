package com.asv.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.asv.entities.Backup;
import com.asv.repositories.BackupRepository;
import com.google.common.collect.Lists;

@Component
public class BackupServiceDaoImpl implements BackupServiceDao {
    @Autowired
    BackupRepository repository;

    @Override
    public Backup saveBackup(Backup backup) {
        backup.setDate(LocalDate.now());
        return repository.save(backup);
    }

    @Override
    public List<Backup> getBackups() {
        return Lists.newLinkedList(repository.findAll());
    }

    @Override
    public Optional<Backup> getBackup(int backupId) {
        return repository.findById(backupId);
    }

    @Override
    public void updateBackup(Backup backup) {
        repository.save(backup);
    }
}
