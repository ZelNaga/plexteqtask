package com.asv.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

import com.asv.entities.Backup;

public interface BackupServiceDao {
    @Retryable(backoff = @Backoff(delay = 10000))
    Backup saveBackup(Backup backup);
    @Retryable(backoff = @Backoff(delay = 10000))
    List<Backup> getBackups();
    @Retryable(backoff = @Backoff(delay = 10000))
    Optional<Backup> getBackup(int backupId);
    @Retryable(backoff = @Backoff(delay = 10000))
    void updateBackup(Backup backup);
}
