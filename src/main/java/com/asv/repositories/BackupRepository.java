package com.asv.repositories;

import org.springframework.data.repository.CrudRepository;

import com.asv.entities.Backup;

public interface BackupRepository extends CrudRepository<Backup, Integer> {
}
