package com.asv.repositories;

import org.springframework.data.repository.CrudRepository;

import com.asv.entities.Todo;

public interface TodoRepository extends CrudRepository<Todo, Integer> {
}
