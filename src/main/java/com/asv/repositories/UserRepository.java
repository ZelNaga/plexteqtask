package com.asv.repositories;

import org.springframework.data.repository.CrudRepository;

import com.asv.entities.User;

public interface UserRepository extends CrudRepository<User, Integer> {
}
