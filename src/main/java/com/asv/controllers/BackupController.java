package com.asv.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.asv.entities.Backup;
import com.asv.service.BackupService;
import com.asv.utils.Views;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
public class BackupController {
    @Autowired
    private BackupService backupService;

    @JsonView(Views.Id.class)
    @RequestMapping(value = "/backups", method = RequestMethod.POST)
    public ResponseEntity<Backup> makeBackup() {
        return ResponseEntity.ok().body(backupService.makeBackup());
    }

    @JsonView(Views.IdAndDateAndName.class)
    @RequestMapping(value = "/backups", method = RequestMethod.GET)
    public ResponseEntity<List<Backup>> getBackup() {
        return ResponseEntity.ok().body(backupService.getBackups());
    }

    @RequestMapping(value = "/exports/{backupId}", method = RequestMethod.GET)
    public ResponseEntity<String> exportBackup(@PathVariable int backupId) throws JsonProcessingException {
        return ResponseEntity.ok()
                      .header("Content-Disposition", String.format("attachment; filename=backup_%s.csv", backupId))
                      .body(backupService.exportBackup(backupId));
    }
}
